import React, { Component, PropTypes } from 'react';

import Button from 'react-islands/components/Button';

export default class SwitcherItem extends Component {
  render() {
    return (
      <Button
        view={this.props.checked ? 'action' : ''}
        theme='islands'
        size='l'
        type='link'
        url={this.props.url}
        onClick={this.props.onItemClick}>
          {this.props.text}
      </Button>
    );
  }
}

SwitcherItem.propTypes = {
  checked: PropTypes.bool,
  url: PropTypes.string,
  onItemClick: PropTypes.func,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ])
};
