import React, { Component } from 'react';

import ControlGroup from '../control-group/ControlGroup';

import SwitcherItem from './__item/SwitcherItem';

export default class Switcher extends Component {
  constructor(props) {
    super(props);

    this.state = {
      links: [
        { url: '#', text: 'POTATOXA', checked: true },
        { url: '#', text: 'VITALOXA', checked: false }
      ]
    };
  }

  onSwitcherClick() {
    this.setState(prevState => {
      return {
        links: prevState.links.map(link => {
          link.checked = !link.checked;
          return link;
        })
      };
    });
  }

  renderLinks() {
    return this.state.links.map((link, id) => (
      <SwitcherItem
        onItemClick={this.onSwitcherClick.bind(this)}
        key={id}
        checked={link.checked}
        url={link.url}
        text={link.text} />
    ));
  }

  render() {
    return (
      <div className="Switcher">
        <ControlGroup>
          {this.renderLinks()}
        </ControlGroup>
      </div>
    );
  }
}
