import React, { Component, PropTypes } from 'react';

import './Heading.css';

/**
 * Generates heading with suggested level or 1
 */
export default class Heading extends Component {
  render() {
    const level = +this.props.level || 1;
    const props = Object.assign({ className: 'heading' }, this.props);

    this.props.className && (props.className += this.props.className);

    return React.createElement(`h${level}`, props, this.props.children);
  }
}

Heading.propTypes = {
  level: PropTypes.oneOfType([
    PropTypes.number, PropTypes.string
  ]),
  children: PropTypes.node
};
