import React, { Component } from 'react';

import Link from 'react-islands/components/Link';

import './HeaderMenu.css';

export default class HeaderMenu extends Component {
  render() {
    return (
      <div className={`header-menu ${this.props.className}`}>
        <Link theme="islands" size="xl" url="#" className="header-menu__tab">Nav 1</Link>
        <Link theme="islands" size="xl" url="#" className="header-menu__tab">Nav 2</Link>
        <Link theme="islands" size="xl" url="#" className="header-menu__tab header-menu__action">GO ACTION</Link>
      </div>
    );
  }
}
