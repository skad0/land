import React, { Component } from 'react';

export default class FooterCopyrights extends Component {
  render() {
    return (
      <div className="footer__copyrights">
        ИП "Иванов Иван Иванович"<br/>ОГРНИП: 311111111111113
      </div>
    );
  }
}
