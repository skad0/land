import React, { Component } from 'react';

import Copyrights from './__copyrights/Copyrights';
import Contacts from './__contacts/Contacts';
import Badges from './__badges/Badges';

import './Footer.css';

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="wrap">
          <div className="footer__column">
            <Copyrights/>
          </div>
          <div className="footer__column">
            <Contacts/>
          </div>
          <div className="footer__column">
            <Badges/>
          </div>
        </div>
      </div>
    );
  }
}
