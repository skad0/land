import React, { Component } from 'react';

import HeaderMenu from '../header-menu/HeaderMenu';
import Logo from '../logo/Logo';

import './Header.css';

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="wrap">
          <Logo className="header__column header__logo" />
          <HeaderMenu className="header__column" />
        </div>
      </div>
    );
  }
}
