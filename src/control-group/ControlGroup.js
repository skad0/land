import React, { Component } from 'react';

export default class ControlGroup extends Component {
  render() {
    return (
      <div className='control-group' role='group'>
        {this.props.children}
      </div>
    )
  }
}

ControlGroup.propTypes = {
  children: React.PropTypes.node
};
