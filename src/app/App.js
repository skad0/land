import React, { Component } from 'react';

import Footer from '../footer/Footer';
import Procs from '../procs/Procs';
import PromoSection from '../promo-section/PromoSection';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <PromoSection />
        <Procs />
        <Footer />
      </div>
    );
  }
}

export default App;
