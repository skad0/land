import React, { Component } from 'react';

import Link from 'react-islands/components/Link';
import Heading from '../heading/Heading';

import './Logo.css';

export default class Logo extends Component {
  render() {
    return (
      <div className={`logo ${this.props.className}`}>
        <Heading level="1">
          <Link theme="islands" size="xl">
            My Perfect Company
          </Link>
        </Heading>
      </div>
    );
  }
}
